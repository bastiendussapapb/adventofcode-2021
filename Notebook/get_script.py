# -*- coding: utf-8 -*-

"""
This script is run by the notebooks to get the path to Scipt.aoc.py
Use "%run get_script.py" at the beginnig of each Notebook.
"""

import sys, os

path = os.path.dirname(os.path.abspath("__file__"))
path = os.path.split(path)[0]
sys.path.append(path)