from typing import List
from numpy import array
import os

# Source of "input_as_string", "input_as_lines", "input_as_ints" : 
#   https://blog.keiruaprod.fr/2021/11/23/getting-ready-for-adventofcode-in-python.html

def input_as_string(filename:str) -> str:
    """returns the content of the input file as a string"""
    with open(data_path(filename)) as f:
        return f.read().rstrip("\n")

def input_as_lines(filename:str) -> List[str]:
    """Return a list where each line in the input file is an element of the list"""
    return input_as_string(filename).split("\n")

def input_as_ints(filename:str) -> List[int]:
    """Return a list where each line in the input file is an element of the list, converted into an integer"""
    lines = input_as_lines(filename)
    line_as_int = lambda l: int(l.rstrip('\n'))
    return list(map(line_as_int, lines))

def data_path(day:str)->str:
    """Return the absolute path of the day so it can be load"""
    path = os.path.dirname(os.path.abspath("__file__"))
    path = os.path.split(path)[0]
    data_path = os.path.join(path, "Data")
    return os.path.join(data_path, day)

def findall_as_ints(lines:str)->List[int]:
    """Findall int in a string, returns as int"""
    from re import findall
    return [int(x) for x in findall('\d+', lines)]

def to_numpy_array(filename:str, shape) -> array:
    """Transform the data into a array of size shape"""
    from re import findall
    return array(findall("\d", input_as_string(filename))).reshape(shape).astype(int)

def sortString(str):
    """Sort a string"""
    return ''.join(sorted(str))
