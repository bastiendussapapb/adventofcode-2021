# Advent of code 2022

This project contains my answers to the 2022 edition of [**Advent of Code**](https://adventofcode.com/2022/about) using Python.<br>
I recommend to see these 3 articles from [Clément Keirua](https://twitter.com/clemkeirua) to prepare the event
 - [Parsing ](https://blog.keiruaprod.fr/2021/11/23/getting-ready-for-adventofcode-in-python.html)
 - [Standard library ](https://blog.keiruaprod.fr/2021/11/28/adventofcode-part2-python-standard-library.html)
 - [Algorithms and data structures ](https://blog.keiruaprod.fr/2021/11/29/adventofcode-part3-algorithms-and-data-structures-competitive-programming.html)

Tips and solutions can be found on the [reddit page of the event.](https://old.reddit.com/r/adventofcode/)

I use [ChatGPT](https://chat.openai.com/chat) to assist me.

### Stucture of the project
------------
├── Data : Contains the data. <br>
├── Notebook : Answer for each day.<br>
├── Script : Global function that I use in multiple notebook (see aoc). <br>
├── LICENSE<br>
└── README      


### Documentation

Some doc I read that might come out handy :

 - [Regrex](https://www.w3schools.com/python/python_regex.asp) : really useful to extract pattern from a string.
 - [Typing](https://docs.python.org/3/library/typing.html)
 - [Itertools](https://docs.python.org/fr/3/library/itertools.html)
 - [Numpy](https://numpy.org/) and [matplotlib](https://matplotlib.org/)
 - [bisect](https://docs.python.org/3/library/bisect.html) : To search in ordered list in $O(\log n)$.
 - [Collections](https://docs.python.org/fr/3/library/collections.html) : counter and dequeue
 - [Queue](https://www.guru99.com/python-queue-example.html) : FIFO and LIFO.
 - [CP-algorithms](https://cp-algorithms.com/)
 - [heapq](https://docs.python.org/3/library/heapq.html) : heap queue algorithm.

